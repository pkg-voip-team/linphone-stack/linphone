linphone (5.3.105-4) unstable; urgency=medium

  * Team upload
  * Disable FlexiAPI because libjsoncpp has wrong libname

 -- Bastian Germann <bage@debian.org>  Tue, 04 Mar 2025 22:04:32 +0100

linphone (5.3.105-3) unstable; urgency=medium

  * Team upload
  * Add directory referenced in .cmake

 -- Bastian Germann <bage@debian.org>  Tue, 04 Mar 2025 18:31:01 +0100

linphone (5.3.105-2) unstable; urgency=medium

  * Team upload
  * Drop shlibs.local

  [ Daniel Kahn Gillmor ]
  * Actually require LINPHONE_DO_APPIMAGE_DOWNLOAD=y for AppImage download

 -- Bastian Germann <bage@debian.org>  Mon, 03 Mar 2025 13:23:16 +0100

linphone (5.3.105-1) unstable; urgency=medium

  * Team upload
  * New upstream version 5.3.105 (Closes: #1099181, #1094046)
  * Refresh copyright info
  * Refresh patches
  * Move from custom ENABLE_SHARED to BUILD_SHARED_LIBS
  * Build without readline patch

 -- Bastian Germann <bage@debian.org>  Sat, 01 Mar 2025 15:36:45 +0100

linphone (5.2.99-6) experimental; urgency=medium

  * debian/patches/use-cxx17-standard.patch: Build the whole project
    with C++17 instead of C++14 due to new requirement from zxing-cpp
    headers (original work by Boyaun Yang for #1058849).

 -- Dennis Filder <d.filder@web.de>  Wed, 03 Jan 2024 20:03:17 +0100

linphone (5.2.99-5) experimental; urgency=medium

  * Build-dep on libzxing-dev instead of on libzxingcore-dev
    (Closes: #1054381).

 -- Dennis Filder <d.filder@web.de>  Sat, 28 Oct 2023 08:50:11 +0200

linphone (5.2.99-4) experimental; urgency=medium

  * Add shlibs.local entry for libbelcard, too.

 -- Dennis Filder <d.filder@web.de>  Sun, 15 Oct 2023 18:49:03 +0200

linphone (5.2.99-3) experimental; urgency=medium

  * Add shlibs.local entry for libortp with version tightened to the
    one that links with the same libbctoolbox that liblinphone links
    with.

 -- Dennis Filder <d.filder@web.de>  Thu, 05 Oct 2023 20:41:58 +0200

linphone (5.2.99-2) experimental; urgency=medium

  * No-change upload to build against fixed mediastreamer2

 -- Bernhard Schmidt <berni@debian.org>  Tue, 03 Oct 2023 10:37:19 +0200

linphone (5.2.99-1) experimental; urgency=medium

  * New upstream release 5.2.99.
  * Bumped soversion to 12 due to ABI break.
  * Try to fix indeterminism issue through directory subtree
    canonicalization.

 -- Dennis Filder <d.filder@web.de>  Mon, 04 Sep 2023 08:53:05 +0200

linphone (5.2.0-4) unstable; urgency=medium

  * Release to unstable.

 -- Bernhard Schmidt <berni@debian.org>  Sat, 29 Jul 2023 22:10:37 +0200

linphone (5.2.0-3) experimental; urgency=medium

  * Add cherry-picked patch to build with GCC 13 (Closes: #1037755)

 -- Dennis Filder <d.filder@web.de>  Sat, 15 Jul 2023 19:35:59 +0200

linphone (5.2.0-2) experimental; urgency=medium

  * Drop transitional package linphone-nogtk (Closes: #1038300)
  * Fix an inlining-related build issue with a non-inlined surrogate
    function L_C_TO_STRING_noinline().

 -- Dennis Filder <d.filder@web.de>  Sat, 08 Jul 2023 14:02:35 +0200

linphone (5.2.0-1) experimental; urgency=medium

  * New upstream release 5.2.0.
  * Bumped soversion to 11 due to ABI break.
  * Comply with Debian policy v4.6.2.

 -- Dennis Filder <d.filder@web.de>  Mon, 19 Dec 2022 19:59:40 +0100

linphone (5.1.65-4) unstable; urgency=medium

  * Disable behind-the-scenes download of the AppImage (Closes: #1031771).
  * Update documentation to highlight limitations in linphonec CLI utility
    and to properly explain how to create an initial ~/.linphonerc file
    (Closes: #1032051).

 -- Dennis Filder <d.filder@web.de>  Thu, 02 Mar 2023 20:10:59 +0100

linphone (5.1.65-3) unstable; urgency=medium

  * Port wrappers/cpp/genwrapper.py to Python 3.11 (Closes: #1029253).

 -- Dennis Filder <d.filder@web.de>  Sat, 21 Jan 2023 09:21:53 +0100

linphone (5.1.65-2) unstable; urgency=medium

  * Release to unstable.

 -- Bernhard Schmidt <berni@debian.org>  Sun, 27 Nov 2022 17:55:28 +0100

linphone (5.1.65-1) experimental; urgency=medium

  * New upstream release 5.1.65.
  * linphone-common is now Multi-Arch: foreign
  * Build should now be reproducible.

 -- Dennis Filder <d.filder@web.de>  Fri, 07 Oct 2022 21:20:31 +0200

linphone (5.0.37-6) unstable; urgency=medium

  * Import upstream fix for crashes on PUBLISH messages without
    SIP-Etags (Closes: #1021043).

 -- Dennis Filder <d.filder@web.de>  Sun, 02 Oct 2022 16:53:48 +0000

linphone (5.0.37-5) unstable; urgency=medium

  * Release to unstable.

 -- Bernhard Schmidt <berni@debian.org>  Mon, 12 Sep 2022 20:25:04 +0200

linphone (5.0.37-4) experimental; urgency=medium

  * Use DEB_HOST_GNU_TYPE instead of DEB_HOST_MULTIARCH to fix FTBFS on i386

 -- Bernhard Schmidt <berni@debian.org>  Mon, 05 Sep 2022 23:27:18 +0200

linphone (5.0.37-3) experimental; urgency=medium

  * Rebuild against libmediastreamer2 with shlibs

 -- Bernhard Schmidt <berni@debian.org>  Mon, 05 Sep 2022 21:08:36 +0200

linphone (5.0.37-2) experimental; urgency=medium

  * Move doxygen from Build-Depends-Indep to Build-Depends

 -- Bernhard Schmidt <berni@debian.org>  Thu, 18 Aug 2022 09:14:19 +0200

linphone (5.0.37-1) experimental; urgency=medium

  [ Dennis Filder ]
  * New upstream release 5.0.37
  * Newly enabled features:
    - Advanced IM
    - LDAP support

 -- Bernhard Schmidt <berni@debian.org>  Tue, 11 Jan 2022 20:57:48 +0100

linphone (4.4.21-2) unstable; urgency=medium

  * Drop d/* related to the dropped/moved linphone binary package
  * Drop cmake version constraint satisfied in oldstable
  * Build with ENABLE_DB_STORAGE (Closes: #983573)
    - Depend on libsoci-dev with C++11 support
    - Replace libsoci-core dependency by libsoci-sqlite3
    - This might fix RC bug #983365 on linphone-desktop
    Thanks to Dennis Filder for debugging this issue and Bill Blough for
    uploading src:soci with C++11 support this quick.
  * Properly set upstream version number by faking git output.
    Thanks to Dennis Filder for the idea (Closes: #983643)

 -- Bernhard Schmidt <berni@debian.org>  Sat, 27 Feb 2021 21:39:31 +0100

linphone (4.4.21-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * New upstream version 4.4.21
  * Drop Tobias Quathamer from Uploaders
  * Drop Mark Purcell from Uploaders
  * Add myself to uploaders

 -- Bernhard Schmidt <berni@debian.org>  Fri, 25 Dec 2020 21:22:28 +0100

linphone (4.4.15-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 4.4.15
  * Rename linphone-nogtk to linphone-cli (there is no gtk version anymore),
    add transitional package
  * Add some dependencies to liblinphone-dev (Closes: #970747)
  * Bump to debhelper-compat 12
  * Adjust Vcs-* for subproject
  * Bump SV to 4.5.1.0, no changes
  * Bump libbctoolbox-dev build-dep
  * Install linphone-daemon and belr grammar

 -- Bernhard Schmidt <berni@debian.org>  Mon, 07 Dec 2020 12:04:23 +0100

linphone (4.4.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 4.4.0
  * Stop building the GTK client, it does not exist anymore
    (Closes: #967597, #967876)
  * Change python build-deps to python3 (Closes: #943162)
  * Bump build-deps to linphone-sdk 4.4
  * d/watch: Switch to upstream primary Gitlab, only use releases

 -- Bernhard Schmidt <berni@debian.org>  Tue, 01 Sep 2020 01:03:37 +0200

linphone (3.12.0-3) unstable; urgency=medium

  * Team upload.
  * Release to unstable
  * Remove obsolete build-dep libupnp-dev
  * Drop liblinphone9.symbols in favour of shlibs
  * Fix FTBFS with GCC-8

 -- Bernhard Schmidt <berni@debian.org>  Thu, 11 Oct 2018 00:17:15 +0200

linphone (3.12.0-2) experimental; urgency=medium

  * Drop unused libsrtp0-dev dependency (Closes: #787183)
  * Drop C++ symbols from liblinphone9.symbols
  * Adjust liblinphone-dev
    - Add libbellesip-dev dependency
    - Add liblinphone++9 dependency
    - Install LinphoneCxx cmake files

 -- Bernhard Schmidt <berni@debian.org>  Sat, 17 Feb 2018 23:49:47 +0100

linphone (3.12.0-1) experimental; urgency=medium

  [ Felix Lechner ]
  * Enabled LDAP; added Build-Depends: libldap2-dev, libsasl2-dev
  * Enabled Lime
  * Added upstream signing key
  * Added pgpsigurlmangle option to watch
  * Removed a URL from watch
  * Replaced Build-Depends: libcunit1-dev with libcunit1-ncurses-dev
  * Require higher version Build-Depend: libbellesip-dev (>= 1.4)
  * Removed duplicates from Build-Depends:
  * Updated Standards-Version: to 3.9.6
  * Made Vcs-Svn: canonical
  * Changed liblinphone7 package name to match SONAME
  * Changed libmediastreamer-base4 package name to match SONAME
  * Added lintian override for embedded srtp library (symbol conflict)
  * Fixed spelling errors
  * Removed /usr/share/gnome from installation dirs for linphone-common

  [ Mark Purcell ]
  * Update debian/watch
  * Fix "linphone Broken Vcs-Svn: header in debian/control"
    updated debian/control (Closes: #743652)
  * Add Build-Depends: polarssl, gsm1

  [ Tzafrir Cohen ]
  * Fix name of signature file in watch file.

  [ Johannes Schauer ]
  * remove patch hunks for mediastreamer2 because mediastreamer2 is no longer
    part of upstream tarball
  * remove build dependency on libpolarssl-dev
  * add build dependency on libbctoolbox-dev
  * add build dependency on libortp-dev (>= 1:0.24.0)
  * add build dependency on libmediastreamer-dev (>= 1:2.11.0)
  * bump dependency on libbellesip-dev to >= 1.4.2
  * remove libortp* and libmediastreamer* from source package as those
    packages moved to their own source packages, respectively
  * upstream renamed README to README.md -> adapt debian/*.docs
  * add build dependency on libmbedtls-dev
  * switch from autotools to cmake
  * rename liblinphone7 to liblinphone9, following upstream soname bump
  * debian/control: switch Vcs-* fields to https
  * debian/control: add Felix Lechner to Uploaders
  * debian/control: bump Standards-Version (no changes required)
  * debian/rules: Use hardening=+all
  * remove lintian-dbg package in favour of automatically generated dbgsym
    package
  * remove debian/linphone.menu as package already provides .desktop file
  * add a patch adding the Keywords entry to the linphone.desktop file
  * add a patch regenerating png images from their svg sources
  * add a patch to update rootca.pem with a more recent version from mozilla
  * run wrap-and-sort -abt
  * change debian/copyright to use DEP5 (Machine-readable debian/copyright)
  * add a patch to build linphone without attempting to download from mozilla
  * add a debian/README.source

  [ Dr. Tobias Quathamer ]
  * New upstream version 3.12.0
    - Fixes "New upstream version available" (Closes: #743474, #889161)
    - Add new Build-Depends: libbelcard-dev, graphviz, libbelr-dev,
      python-pystache, python-six
    - Refresh patches
  * Update d/watch to new github location
  * Update d/copyright
  * Wrap d/rules and add ENABLE_ROOTCA_DOWNLOAD=NO
  * Add more files to d/liblinphone-dev.install
  * Remove patch offline-mode, should not be needed anymore
  * Disable spelling errors patch for now
  * Use debhelper's dbgsym-migration
  * Update Build-Depends: xxd moved from vim-common to xxd.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #887381)
  * Switch Vcs-URLs to salsa.d.o (Closes: #855421)

  [ Bernhard Schmidt ]
  * Build legacy GTK UI
  * Create empty coreapi/liblinphone_gitversion.h to fix FTBFS
  * New binary package liblinphone++9

 -- Bernhard Schmidt <berni@debian.org>  Thu, 15 Feb 2018 23:03:32 +0100

linphone (3.6.1-2.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Add libav11.patch fix compilation against libav11 (Closes: #758017)

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 16 Aug 2014 11:01:47 -0400

linphone (3.6.1-2.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Upload to unstable now that libav10 is in the archive (Closes: #739314)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 14 May 2014 00:25:23 +0200

linphone (3.6.1-2.2) experimental; urgency=low

  * Non-maintainer upload.
  * Add libav10.patch and compile against libav10 (Closes: #739314)

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 26 Mar 2014 19:51:02 -0400

linphone (3.6.1-2.1) unstable; urgency=high

  * Non-maintainer upload.
  * Apply Sebastian Ramacher's patch to fix FTBFS (Closes: #720668).

 -- Luk Claes <luk@debian.org>  Wed, 11 Sep 2013 19:08:43 +0200

linphone (3.6.1-2) unstable; urgency=low

  * Add Build-Depends: libudev-dev [linux-any]

 -- Mark Purcell <msp@debian.org>  Sun, 04 Aug 2013 07:44:54 +1000

linphone (3.6.1-1) unstable; urgency=low

  * New upstream release
   - NEW packages liblinphone5, libmediastreamer-base3, libortp9 - match soname

  [ Felix Lechner ]
  * New upstream version
  * Update Standards-Version: 3.9.4
  * Port Linphone to the new libeXosip API v4
    - Fixes "FTBFS: sal_eXosip2.c:49:2: error: too few arguments to function
    'eXosip_guess_localip'" (Closes: #710638)
    - Fixes "FTBFS for libexosip2-4 API" (Closes: #709860)
  * Add dh_autoreconf (while upstream patch is pending)
  * Enable IPv6
  * Install documentation README.Debian about 'cv_addr_book.pl'
  * Update package names to reflect library SONAMEs
  * Drop Build-Depends: libsdl1.2-dev, libsamplerate0-dev, libglib2.0-dev
  * Add Build-Depends: automake, quilt, libudev-dev, libpcap-dev, libsoup2.4,
    libupnp-dev, libssl-dev, libxext-dev, mesa-common-dev, libcunit1-dev,
    libspandsp-dev, vim-common, sgmltools-lite, libsqlite3-dev, libpulse-dev,
    libopus-dev

  [ Mark Purcell ]
  * Add additional Build-Dpends:

 -- Mark Purcell <msp@debian.org>  Wed, 31 Jul 2013 20:25:44 +1000

linphone (3.5.2-11) experimental; urgency=low

  * Fix "Missing Media encryption" --enable-zrtp (Closes: #671815)
  * Fix "not compiled with vp8 video codec"
    - Added Build-Depends: libvpx-dev (Closes: #688855)
  * Fix "ancient automake9" dropped Build-Depends: (Closes: #688862)
  * Fix "build with notification" added Build-Depends: (Closes: #688856)
  * Update libortp-dev: fix duplicate-{short,long}-description
  * fixed copyright-refers-to-symlink-license - debian/copyright

 -- Mark Purcell <msp@debian.org>  Tue, 09 Oct 2012 21:43:54 +1100

linphone (3.5.2-10) unstable; urgency=low

  * wheezy polish
  * Update Standards-Version: 3.9.3
  * Fix spelling-error-in-binary usr/bin/mediastream comand command
  * Added manpage-section-mismatch.patch
  * debian/compat -> 9 - hardening & multiarch

 -- Mark Purcell <msp@debian.org>  Sun, 24 Jun 2012 22:47:30 +1000

linphone (3.5.2-9) unstable; urgency=low

  * NEW package linphone-nogtk - CLI interface without libgtk
    - Fixes "linphonec segfaults without framebuffer"  (Closes: #404113)
    - Fixes "please make a package separate for CLI" (Closes: #674221)
    - Fixes "please rework the deb dependencies" (Closes: #675389)
  * Fix "FTBFS: linphonec.c:1432: undefined reference to
    `rl_attempted_completion_over'" patch from Sebastian Ramacher
    (Closes: #669501)
  * Refresh Uploaders: last 12 months contribution
  * Add Build-Depends: libncurses5-dev, libreadline6-dev

 -- Mark Purcell <msp@debian.org>  Sat, 02 Jun 2012 11:25:11 +1000

linphone (3.5.2-8) unstable; urgency=high

  * Urgency high for RC bug
  * libortp-dev Depends: libsrtp0-dev [!hurd-any !sparc]
    - Fix "libortp-dev depends on libsrtp0-dev on less archs than
    required" (Closes: #662951)

 -- Mark Purcell <msp@debian.org>  Mon, 12 Mar 2012 21:01:12 +1100

linphone (3.5.2-7) unstable; urgency=low

  * Exclude libsrtp-dev [!hurd !sparc] only where it FTFBS (Closes: #661645)

 -- Mark Purcell <msp@debian.org>  Wed, 29 Feb 2012 19:32:47 +1100

linphone (3.5.2-6) unstable; urgency=low

  * [!linux] --disable-video (Closes: #661336)
  * Build-Depends: libsrtp-dev [linux-any !sparc] - see Bug 628583

 -- Mark Purcell <msp@debian.org>  Tue, 28 Feb 2012 22:59:05 +1100

linphone (3.5.2-5) unstable; urgency=low

  * Drop obsolete videodev.h (v4l1-headers) - mediastreamer2/configure works
  * Drop ./autogen.sh

 -- Mark Purcell <msp@debian.org>  Tue, 28 Feb 2012 22:19:24 +1100

linphone (3.5.2-4) unstable; urgency=low

  * Drop dh_autoreconf

 -- Mark Purcell <msp@debian.org>  Mon, 27 Feb 2012 21:35:24 +1100

linphone (3.5.2-3) unstable; urgency=low

  * Fix FTBFS on [!linux] - thanks pinotree (Closes: #661336)
    - --disable-{alsa,v4l}[!linux]
    - Build-Depends srtp,asound2,v4l[linux-any]
    - Added linux_headers_linux_only.diff
    - Call autogen.sh (autoreconf)
  * Cleanup obsolete versioned Build-Depends
  * --disable-ssl to avoid unintended linking
  * Drop obsolete debian/README.*
  * source/format 3.0 (quilt)

 -- Mark Purcell <msp@debian.org>  Mon, 27 Feb 2012 20:48:58 +1100

linphone (3.5.2-2) unstable; urgency=low

  * Upload to unstable - debian-release endorsed
  * Fix "[libortp8] "multiples profiles" in extended descriptions"
    Updated debian/control (Closes: #653474)

 -- Mark Purcell <msp@debian.org>  Sun, 26 Feb 2012 10:01:21 +1100

linphone (3.5.2-1) experimental; urgency=low

  * New upstream release
  * Switch to dh
  * debian/compact -> 8

 -- Mark Purcell <msp@debian.org>  Sat, 25 Feb 2012 17:10:22 +1100

linphone (3.5.1-1) experimental; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 18 Feb 2012 16:20:31 +1100

linphone (3.5.0-2) experimental; urgency=low

  * Build-Depends: libexosip2-dev (>> 3.5.0)

 -- Mark Purcell <msp@debian.org>  Sun, 22 Jan 2012 16:57:12 +1100

linphone (3.5.0-1) experimental; urgency=low

  * New upstream release

  [ Kilian Krause ]
  * Fix descriptions - thanks to Filipus Klutiero (Closes: #631227)

  [ Tzafrir Cohen ]
  * Add a dbg package.

  [ Mark Purcell ]
  * Fix "please add support for armhf" patch from Konstantinos (Closes: #645799)

 -- Mark Purcell <msp@debian.org>  Mon, 26 Dec 2011 11:40:28 +1100

linphone (3.3.2-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't ship .la files (Closes: #622543).

 -- Luk Claes <luk@debian.org>  Sun, 26 Jun 2011 16:14:34 +0200

linphone (3.4.3-1) experimental; urgency=low

  * New upstream release
  * Upstream dropped references to {man,bin}/linphone-3
  * NEW packages: liblinphone4 libmediastreamer1 - package-match-sonames
  * Drop oldstable Replaces: Provides:

 -- Mark Purcell <msp@debian.org>  Sat, 11 Jun 2011 11:59:06 +1000

linphone (3.3.2-4) unstable; urgency=low

  * Fix "FTBFS: msv4l.c:33:28: fatal error: linux/videodev.h: No such
    file or directory" - unbuntu fix (Closes: #621970)
  * Added Build-Depends: libxv-dev

 -- Mark Purcell <msp@debian.org>  Sat, 11 Jun 2011 11:21:44 +1000

linphone (3.3.2-3ubuntu2) natty; urgency=low

  * Add mediastreamer2/linux/videodev.h from 2.6.32 kernel, as 2.6.38 dropped
    V4L1. This is an ugly workaround to make the package build again (it will
    use V4L2 at runtime). The current package (as well as the latest upstream
    version 3.4.3) hardwires libv4l1. It uses some internals which
    <libv4l1-videodev.h> does not provide, so we cannot fall back to this.

 -- Martin Pitt <martin.pitt@ubuntu.com>  Mon, 11 Apr 2011 15:23:10 +0200

linphone (3.3.2-3) unstable; urgency=low

  * Update Standards Version 3.9.1 - no changes
  * Make explict -dev versioned depends - weak-library-dev-dependency
  * Added linphone.1 & linphone-3.1 - binary-without-manpage
  * Dropped sipomatic.1 - binary dropped upstream

 -- Mark Purcell <msp@debian.org>  Sat, 14 Aug 2010 11:16:51 +1000

linphone (3.3.2-2) unstable; urgency=low

  * Tighten shlibs version dependss; libmediastreamer0, libortp8, liblinphone3
    - Fixes "linphone does not start (undefined symbol in
    /usr/lib/liblinphone.so.3)" (Closes: #592183)

 -- Mark Purcell <msp@debian.org>  Sun, 08 Aug 2010 13:11:41 +1000

linphone (3.3.2-1) unstable; urgency=low

  * New upstream release
  * Refresh debian/watch to gnu.org

 -- Mark Purcell <msp@debian.org>  Wed, 07 Jul 2010 13:00:37 +1000

linphone (3.3.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 07 Jun 2010 22:14:41 +1000

linphone (3.3.0-2) unstable; urgency=low

  * Make SRTP support explicit (Closes: #583008)
    - Build-Depends: libsrtp-dev [alpha amd64 armel hppa i386 mips mipsel
    powerpc s390]
    - Build-Conflicts: libssl-dev - uses none of its symbols
    - libortp-dev Depends: libsrtp0-dev [alpha amd64 armel hppa i386 mips
    mipsel powerpc s390]

 -- Mark Purcell <msp@debian.org>  Sun, 30 May 2010 18:35:15 +1000

linphone (3.3.0-1) unstable; urgency=low

  * New upstream release

  * debian/control: s/is a is an/is an/ (Closes: #582661)
  * debian/control s/It main/Its main/ (Closes: #582665)
  * configure --disable-strict (Closes: 561708)
  * Cleanup arches libv4l-dev libasound2-dev (Closes: #542595)
  * Cleanup debian/watch
  * Drop fix_desktop_section.dpatch - included upstream
  * Drop desktop-icon.dpatch - included upstream
  * Drop always_ipv4_for_ipv4_hosts.dpatch - included upstream
  * Drop dpatch
  * Upstream dropped /usr/bin/sipomatic

 -- Mark Purcell <msp@debian.org>  Sun, 23 May 2010 10:04:07 +1000

linphone (3.2.1-2) unstable; urgency=low

  [ Mark Purcell ]
  * Pickup upstream user changelog: NEWS

  [ Lionel Elie Mamane ]
  * Identify with IPv4 address to IPv4 host even if it has an AAAA DNS RR
    (Closes: #578950).
  * Enable parallel builds
  * Disable alsa on GNU/kFreeBSD and GNU/Hurd (Closes: #542595).
  * Bump up Standards-Version to 3.8.4
  * Migrate Build-Depends to libreadline-dev
    instead of libreadline5-dev (Closes: #553805)
  * Depend on precise liblinphone3 version (Closes: #545093).
  * Don't install badly out-of-date linphone(1) manpage,
    do install linphonecsh(1)
  * Correct location of icon in .desktop file (Closes: #519679)
  * Build-Depend on libv4l-dev
  * Add myself as Uploader

 -- Lionel Elie Mamane <lmamane@debian.org>  Mon, 26 Apr 2010 20:59:06 +0200

linphone (3.2.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Wed, 14 Oct 2009 08:26:02 +1100

linphone (3.2.0-1) unstable; urgency=low

  * New upstream release
  * Add Build-Depends: libgsm1-dev
    - Fixes "[linphone] gsm codec not supported anymore?" (Closes: #537054)
  * Fix "FTBFS twice in a row" Patch from Peter Green, thks
    (Closes: #530728)
  * Minor debian/watch changes
  * Fix "depends on libraw1394-8 which is unavailable" Rebuilt (Closes: #518574)

 -- Mark Purcell <msp@debian.org>  Fri, 09 Oct 2009 08:34:14 +1100

linphone (3.1.2-2) unstable; urgency=low

  [ Kilian Krause ]
  * Remove -N from wget args in get-orig-source target as -O is already
    used.

  [ Lionel Elie Mamane ]
  * linphone: Fix file conflict with linphone-common (<= 3.1.2-1)
    (Closes: #528076)

 -- Lionel Elie Mamane <lmamane@debian.org>  Wed, 27 May 2009 11:39:51 +0200

linphone (3.1.2-1) unstable; urgency=low

  * New upstream release.
  * Add get-orig-source target to debian/rules
  * Fix menu-icon-missing by installing
    /usr/share/pixmaps/linphone/linphone2.xpm directly into linphone package

 -- Kilian Krause <kilian@debian.org>  Sat, 09 May 2009 23:01:20 +0200

linphone (3.1.1-2) unstable; urgency=low

  * Fix "linphone_3.1.1-1(mips/unstable): FTBFS on mips: missing
    intltool" added Build-Depends: intltool (Closes: #525052)
  * Fix "symbol lookup error: /usr/lib/liblinphone.so.3: undefined"
    set shlibs args "libmediastreamer0 (>= 3)" (Closes: #520250)

 -- Mark Purcell <msp@debian.org>  Wed, 22 Apr 2009 08:01:08 +1000

linphone (3.1.1-1) unstable; urgency=low

  * New upstream release
    - Fixes "new version 3.1.0 is available" (Closes: #521128)
  * Fix "should depend on host" Added Depends: (Closes: #524620)
  * Build-Depends & Description: changes from Simon Morlat (upstream)
  * Add linphonecsh to linphone-nox package

 -- Mark Purcell <msp@debian.org>  Tue, 21 Apr 2009 22:34:41 +1000

linphone (3.0.0-3) unstable; urgency=low

  * Fix "spurious gnome-applets" Removed Build-Depends: (Closes: #520133)

 -- Mark Purcell <msp@debian.org>  Wed, 18 Mar 2009 07:29:23 +1100

linphone (3.0.0-2) unstable; urgency=low

  * Rebuild against libavcodec-dev=3:0.svn20090303-1 (sid)

 -- Mark Purcell <msp@debian.org>  Tue, 10 Mar 2009 20:57:04 +1100

linphone (3.0.0-1) unstable; urgency=low

  [ Patrick Matthäi ]
  * Add missing description for fix_desktop_section.dpatch.
    Thanks lintian.

  [ Mark Purcell ]
  * New Upstream Release
    - Fixes "new version 3.0.0 available" (Closes: #505566)
    - Fixes "linphone - FTBFS: error: ffmpeg/avcodec.h:"  (Closes: #517835)
    - libosip2 transition
  * Switch to cdbs
  * NEW packages libortp8, liblinphone3 - soname bump
  * NEW packages libortp-dev, liblinphone-dev, libmediastreamer-dev
  * Confilicts:/ Provides:/ Replaces:
        libortp7-dev, liblinphone2-dev, libmediastreamer0-dev
  * Standards-Version: 3.8.0  - fixed by above

 -- Mark Purcell <msp@debian.org>  Mon, 09 Mar 2009 09:54:17 +1100

linphone (2.1.1-1) unstable; urgency=low

  * Added missing build dependency on libspeexdsp-dev.
    Closes: #474851

  [ Faidon Liambotis ]
  * New upstream version.
    - Adapt debian/libortp7-dev.install to work with libortp 0.14.2 and future
      versions.
  * Do not change config.sub/.guess on clean (eliminates lintian warning).

 -- Faidon Liambotis <paravoid@debian.org>  Sun, 27 Apr 2008 18:21:57 +0300

linphone (2.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Samuel Mimram <smimram@debian.org>  Thu, 14 Feb 2008 21:46:23 +0100

linphone (2.0.1-3) unstable; urgency=low

  * Added a shlibs for libmediastreamer0, closes: #454900.

 -- Samuel Mimram <smimram@debian.org>  Sat, 08 Dec 2007 17:41:58 +0000

linphone (2.0.1-2) unstable; urgency=low

  * Add missing dependency on doxygen.

 -- Samuel Mimram <smimram@debian.org>  Thu, 06 Dec 2007 12:14:39 +0000

linphone (2.0.1-1) unstable; urgency=low

  [ Kilian Krause ]
  * New upstream release.
  * Conflict with libortp5-dev to fix conflicting file
    (/usr/include/ortp/event.h). (Closes: #454281)

  [ Samuel Mimram ]
  * liblinphone2-dev conflicts with liblinphone1-dev.

 -- Samuel Mimram <smimram@debian.org>  Wed, 05 Dec 2007 11:24:25 +0000

linphone (2.0.0-1) unstable; urgency=low

  * New upstream release, closes: #453185.
  * New soname for liblinphone.
  * Removed fix_segfault.dpatch, ingerated upstream.
  * Updated watch file.

 -- Samuel Mimram <smimram@debian.org>  Thu, 29 Nov 2007 11:47:30 +0000

linphone (1.99.0-3) unstable; urgency=high

  * Backport segfault fix from CVS. (Closes: #445462)
  * High urgency due to fixing RC bug.
  * Fix wrong menu section (Closes: #444898)
  * Add myself to Uploaders to get rid of NMU nag.
  * Fix "Long descriptions contains short description."
  * Fix "desktop-entry-invalid-category Application"

 -- Kilian Krause <kilian@debian.org>  Sun, 07 Oct 2007 00:00:52 +0000

linphone (1.99.0-2) unstable; urgency=low

  [ Kilian Krause ]
  * Fix debian/watch and get-orig-source target

  [ Samuel Mimram ]
  * Rebuild against latest libosip2, closes: #441896.

 -- Samuel Mimram <smimram@debian.org>  Wed, 12 Sep 2007 15:34:40 +0200

linphone (1.99.0-1) unstable; urgency=low

  * New upstream release.
  * Added a build-dependency on libexosip2-dev.
  * New soname for libortp.
  * Correctly builds with latest libosip2, closes: #439481.
  * Hide manual menu entry since none is available, closes: #287159.

 -- Samuel Mimram <smimram@debian.org>  Thu, 30 Aug 2007 16:23:08 +0200

linphone (1.7.1-3) unstable; urgency=low

  * Make liblinphone1-dev depend on libmediastreamer0-dev (thanks Alex Samad),
    closes: #433430.
  * Install nowebcamCIF.jpg in linphone-common.
  * Don't ignore errors on make clean.

 -- Samuel Mimram <smimram@debian.org>  Wed, 18 Jul 2007 10:20:53 +0200

linphone (1.7.1-2) unstable; urgency=low

  * Add --disable-strict option to configure, closes: #427194.

 -- Samuel Mimram <smimram@debian.org>  Sun, 10 Jun 2007 22:22:47 +0200

linphone (1.7.1-1) unstable; urgency=low

  * New upstream release.
  * The gnome applet was removed from upstream sources, not installing it
    anymore.
  * Removed desktop_icon.dpatch, gnome_applet_dir.dpatch and
    sip-max-forwards.dpatch which are either obsolete or integrated upstream.

 -- Samuel Mimram <smimram@debian.org>  Tue, 24 Apr 2007 23:54:23 +0200

linphone (1.6.0-2) unstable; urgency=low

  * Disable arts support, closes: #397815.

 -- Samuel Mimram <smimram@debian.org>  Fri, 23 Mar 2007 00:06:37 +0100

linphone (1.6.0-1) experimental; urgency=low

  [ Samuel Mimram ]
  * New upstream release.
  * Added sip-max-forwards.dpatch to correctly set SIP maximum forwards
    number, closes: #413193.
  * Updated upstream url in watch and rules.

  [ Kilian Krause ]
  * Fix building arch=all in binary-arch target.

 -- Samuel Mimram <smimram@debian.org>  Thu,  8 Mar 2007 21:43:06 +0100

linphone (1.5.1-1) unstable; urgency=low

  * New upstream release.
  * Enable video support.
  * Fix mismatched #endif in mscommon.h, closes: #398307.

 -- Samuel Mimram <smimram@debian.org>  Wed, 15 Nov 2006 10:34:50 +0000

linphone (1.5.0-1) unstable; urgency=low

  * New upstream release.
  * Removed ice.dpatch and stun_sparc.dpatch, integrated upstream.

 -- Samuel Mimram <smimram@debian.org>  Wed, 11 Oct 2006 11:33:53 +0000

linphone (1.4.1-2) unstable; urgency=low

  * Added ice.dpatch to fix the FTBFS on amd64 (thanks Jérémy Bobio)
    and stun_sparc.dpatch to fix the FTBFS on sparc, closes: #390009.

 -- Samuel Mimram <smimram@debian.org>  Sat, 30 Sep 2006 17:16:37 +0000

linphone (1.4.1-1) unstable; urgency=low

  * New upstream release.
  * Removed no_developer_docs.dpatch, 00mscodec_null_name.dpatch,
    10mscodec_null_strcmp.dpatch, gcc-4.1.dpatch and
    world_readable_passwords.dpatch, not needed anymore.
  * New packages: libmediastreamer0 and libmediastreamer0-dev.
  * New SONAME for libortp.
  * Updated watch file.
  * Acknowledge NMU, closes: #382616.

 -- Samuel Mimram <smimram@debian.org>  Tue, 19 Sep 2006 09:55:19 +0000

linphone (1.3.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Always include config.h in sndcard.h (Closes: #382616).
  * Make binNMUable.

 -- Luk Claes <luk@debian.org>  Sat, 16 Sep 2006 12:16:32 +0200

linphone (1.3.5-1) unstable; urgency=low

  [ Kilian Krause ]
  * Bump speex build-dep to 1.1.12 as configure requests.

  [ Samuel Mimram ]
  * New upstream release.
  * Added no_developer_doc.dpatch in order not to build some parts of the
    documentation which don't build for now (we didn't ship them anyway),
    closes: #365523. We need to regenerate configure and Makefiles for this.
  * Added world_readable_passwords.dpatch to change the permissions of the
    configuration file to 600 (thanks Alec Berryman). This might be too strict
    but at least it is safe, closes: #361913.
  * Updated desktop_icon.dpatch to correct the location of the icon in the
    linphone.desktop file, closes: #346435.
  * Updated standards version to 3.7.2, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Mon, 15 May 2006 09:02:05 +0000

linphone (1.3.3-1) unstable; urgency=low

  * New upstream release.
  * Added gcc-4.1.dpatch in order to compile with gcc 4.1, closes: #358068.

 -- Samuel Mimram <smimram@debian.org>  Tue, 21 Mar 2006 10:46:37 +0000

linphone (1.3.1-1) unstable; urgency=low

  [ Samuel Mimram ]
  * New upstream release.
  * New maintainer Debian VoIP Team, added myself to uploaders.
  * New soname for libortp.
  * Removed addressbook_entries.dpatch, integrated upstream.

  [ Kilian Krause ]
  * debian/rules: Add get-orig-source target.

 -- Samuel Mimram <smimram@debian.org>  Wed, 15 Mar 2006 23:18:57 +0000

linphone (1.2.0-3) unstable; urgency=low

  * Addd desktop_icon.dpatch to correct the icon location in linphone.desktop,
    closes: #346435.
  * Added addressbook_entries.dpatch to make addressbook work as expected when
    clicking on an entry (thanks Emiliano Necciari).

 -- Samuel Mimram <smimram@debian.org>  Fri, 20 Jan 2006 23:42:23 +0100

linphone (1.2.0-2) unstable; urgency=low

  * Updated liblinphone1's shlibs, closes: #345457.

 -- Samuel Mimram <smimram@debian.org>  Sat, 31 Dec 2005 19:24:49 +0100

linphone (1.2.0-1) unstable; urgency=low

  * New upstream release.
  * Primary selection isn't destroyed anymore, closes: #309374.
  * Linphonec now handles ctrl+D, closes: #333423.
  * Swedish translation integrated upstream, closes: #340432.
  * Removed pl_utf8 patch, integrated upstream.
  * Soname bump: libortp2 instead of libortp0.

 -- Samuel Mimram <smimram@debian.org>  Sun, 18 Dec 2005 12:39:06 +0100

linphone (1.1.0-2) unstable; urgency=low

  * Added a shlibs to version the dependencies on liblinphone1 and updated
    libortp0's one, closes: #327795.
  * Added pl_utf8 patch to correct the charset of the polish translation,
    closes: #327457.
  * Installing ortp.pc in libortp0-dev.

 -- Samuel Mimram <smimram@debian.org>  Mon, 12 Sep 2005 12:02:50 +0200

linphone (1.1.0-1) unstable; urgency=low

  * New upstream release.
  * IPv6 should work now, closes: #301874.
  * Clearing of passwords should work now, closes: #321064.
  * Corrected typos in german translation, closes: #313999.
  * Removed addrbook_sip_addr patch (integrated upstream).
  * Added yelp to linphone's Suggests, closes: #325863.

 -- Samuel Mimram <smimram@debian.org>  Thu,  8 Sep 2005 19:37:27 +0200

linphone (1.0.1-6) unstable; urgency=low

  * Build-depending on libjack0.100.0-dev instead of libjack0.80.0-dev,
    closes: #317211.
  * Updated standards version to 3.6.2.

 -- Samuel Mimram <smimram@debian.org>  Mon, 18 Jul 2005 21:31:20 +0200

linphone (1.0.1-5) unstable; urgency=low

  * Added a patch to check sip addresses format when adding contacts in
    address book, closes: #305078.

 -- Samuel Mimram <smimram@debian.org>  Sat, 30 Apr 2005 13:58:35 +0200

linphone (1.0.1-4) unstable; urgency=low

  * Added a perl script and a README.Debian to explain how to convert old
    adress book entries to new format, closes: #303819.

 -- Samuel Mimram <smimram@debian.org>  Sun, 10 Apr 2005 17:57:39 +0200

linphone (1.0.1-3) unstable; urgency=low

  * Tightened the shlibs of libortp0 to 1.0.1 version.

 -- Samuel Mimram <smimram@debian.org>  Thu, 31 Mar 2005 18:11:23 +0200

linphone (1.0.1-2) unstable; urgency=low

  * Added the 10mscodec_null_strcmp patch to make some more checks for NULL
    pointers. I hope this will solve the crash on startup, closes: #301494.

 -- Samuel Mimram <smimram@debian.org>  Wed, 30 Mar 2005 11:19:04 +0200

linphone (1.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Samuel Mimram <smimram@debian.org>  Wed, 23 Mar 2005 12:50:48 +0100

linphone (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * The code was reorganized to be cleaner, some libraries have been merged.
  * Big-endian architectures are now supported, closes: #275738, #284079.
  * Sound should not be decreased on startup anymore, closes: #299227.
  * Samuel Thibault's patches were integrated upstream (many thanks),
    closes: #297213, #297215, #297622.
  * Put the rings in a separate package (linphone-common) to save disk space
    on the Debian mirrors.
  * Using dh_desktop to register the .desktop file.
  * Added -g to the CFLAGS.

 -- Samuel Mimram <smimram@debian.org>  Mon, 21 Mar 2005 18:11:06 +0100

linphone (0.12.2-2) unstable; urgency=low

  * Removed all big-endian archs since they are not supported by this version
    of linphone (please wait until the release of the 1.0 version).
  * Now using other packages' shlibs with dh_shlibdeps.
  * Switched to dpatch to handle the patches.
  * Install /usr/share/doc/linphone-nox/linphonec and corrected the error
    message accordingly to display the right path, closes: #270935.
  * Applied a patch to have IPv6 really enabled in the libmediastreamer (thanks
    Henrik Riomar and Fabio Massimo Di Nitto).
  * Check for NULL arguments in mscodec.c.
  * Updated the config.guess and config.sub.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Fri, 10 Sep 2004 11:20:03 +0200

linphone (0.12.2-1) unstable; urgency=low

  * Initial Release, closes: #68243.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Wed, 30 Jun 2004 13:58:16 +0200
